本项目托管自己使用的U盘多系统安装引导的grub.cfg以及使用的主题

# 使用方法
按照[我的博客](http://my.oschina.net/abcfy2/blog/491140)的方法安装grub，然后将此项目目录下的所有文件放在grub安装的目录下即可，我的U盘grub目录在``boot/grub``

注意编辑``grub.cfg``文件，以适应你的iso路径和文件名。我的iso存放路径为``boot/iso``

参考命令:
```bash
# 对应的把下面的/dev/sda1替换成你的U盘的ESP分区，比如/dev/sdb1
sudo mount /dev/sda1 /mnt/  -o uid=$USER,gid=$USER,utf8=1
cp -R grub.cfg themes/ /mnt/boot/grub/
```

任何建议以及PR都欢迎。

默认主题效果图:

![](https://static.oschina.net/uploads/img/201508/13124155_8prd.png)

根据[Tuxkiller V2](http://gnome-look.org/content/show.php/Grub2+Theme+Tuxkiller+V2?content=169520)这个grub主题修改而成

# 目前支持的镜像
**注: 目前所有的镜像测试的都是64bit镜像，我的精力有限，并没有测试32bit镜像。如果你发现有问题，欢迎递交issue或者pull request修复**
* ubuntu & ubuntu server: 已测试12.04/14.04/16.04的64bit镜像
* ubuntukylin: 已测试14.04/16.04镜像
* deepin 15
* debian LiveCD/liveDVD/DVD: 已测试debian8,7及以下未测试
* centos DVD: DVD版镜像包含DVD/Everything/Minimal/Netinstall镜像，已测试6/7(6版本安装问题请看最后的[已知问题](#centos6-dvd镜像安装问题_7))
* centos LiveCD: LiveCD版镜像包含LiveCD/LiveDVD镜像，已测试7(6的LiveCD暂时没好办法支持，请参看最后的[已知问题](#centos6-livecd不支持_6))

将你的iso镜像放在U盘的`boot/iso`目录中，编辑`boot/grub/grub.cfg`文件，替换中间的那些iso文件对应到你的镜像名即可。不存在的镜像会隐藏掉对应的引导条目。

> Grub2目前不支持`*`通配这种形式，无法通过CentOS-\*这种形式搞定镜像名的匹配，因此目前只能人工指定对应的文件名的方式引导对应的镜像。欢迎提交PR解决这个问题。

# 已知问题
## 不要在虚拟机下测试安装
虚拟机只能用来测试引导项和显示效果，用来安装很可能失败!

## Ubuntu Server会检测不到光盘 
Google了一圈，这个问题是普遍现象，提示找不到光盘的时候进入shell，将``/media/boot/iso``下的ubuntu server的镜像挂载至``/cdrom``，继续即可

```bash
# 对应将下面的/dev/sdb1,ubuntu-14.04.3-server-amd64+mac.iso分别改成你的U盘分区和镜像名
# 欢迎pull request解决这个问题，无需手工挂载镜像
mount /dev/sdb1 /media
mount -o loop /media/boot/iso/ubuntu-14.04.3-server-amd64+mac.iso /cdrom
```

## centos6 LiveCD不支持
google了半天，似乎是因为centos 6的的内核实在太过于老旧，根本不支持从iso文件中直接加载`squashfs.img`，所以根本没办法引导livecd的iso，只能将iso解压后添加加载项。这样的话和最初设计不符，所以暂时去掉了centos 6的LiveCD的支持，期待有PR能解决这个问题

## centos7 livecd语言是英文
google了半天找文档，实在找不到centos livecd的内核参数，勉强找到个`LANG`参数，是传递给安装器的，只有安装器打开默认是中文，livecd/dvd系统进去依旧是英文。另外发现CentOS6/7使用的`squashfs.img`引导器是[dracut](https://fedoraproject.org/wiki/Dracut)，而dracut有个[rd.locale.LANG](https://www.kernel.org/pub/linux/utils/boot/dracut/dracut.html#_i18n)参数是控制语言的，但是在LiveCD中没用。希望有pull request解决。

## centos6 DVD镜像安装问题
centos6 DVD镜像在安装时会提示找不到`install.img`文件，让选择这个文件的路径。需要提前将镜像中的`images/`目录解压出来，放到U盘中，在让你选择image type的界面的时候选择`hard drive`并指向`images/`目录所在的**父目录**即可，如`/dev/sdb1/iso/`

如果能联网，也可以选择`URL`一项，启用网络连接后，在`URL Setup`界面中填入centos镜像仓库的路径即可，比如清华大学的镜像仓库:

    https://mirrors.tuna.tsinghua.edu.cn/centos/6/os/x86_64/

当安装器找到`images/install.img`即可下一步了，期待pull request解决这个问题。
